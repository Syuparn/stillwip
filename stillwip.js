// ==UserScript==
// @name        still_wip
// @namespace   https://github.com/syuparn/
// @version     0.1.0
// @description alert when you wrongly ask for review in WIP Merge Request
// @author      syuparn
// @match       https://gitlab.com/*/merge_requests/*
/* load jQuery */
// @require https://code.jquery.com/jquery-3.5.1.slim.min.js
// ==/UserScript==

(function () {
  'use strict';
  String.prototype.isWip = function () {
    return this.startsWith('WIP:');
  }

  // get MR title. if not found, return blank string instead.
  function getTitle() {
    const metaElements = document.getElementsByTagName('meta');
    // HACK: convert elements to array by Array.prototype.slice
    const titleElements = Array.prototype.slice.call(metaElements).filter(
      e => e.getAttribute('property') === 'og:title'
    );
    if (titleElements.length === 0) {
      return '';
    }
    return titleElements[0].content || '';
  }

  function showAlert() {
    const WIP_STRING = [
      '🌕🌑🌑🌑🌕🌑🌕🌕🌕🌕🌕🌑🌕🌕🌕🌕🌑',
      '🌕🌑🌕🌑🌕🌑🌑🌑🌕🌑🌑🌑🌕🌑🌑🌑🌕',
      '🌕🌑🌕🌑🌕🌑🌑🌑🌕🌑🌑🌑🌕🌕🌕🌕🌑',
      '🌕🌑🌕🌑🌕🌑🌑🌑🌕🌑🌑🌑🌕🌑🌑🌑🌑',
      '🌑🌕🌑🌕🌑🌑🌕🌕🌕🌕🌕🌑🌕🌑🌑🌑🌑'
    ].join('\n');
    alert(WIP_STRING);
  }

  // InputDetector detects when string pattern is input.
  function InputDetector(pattern) {
    this.pattern = pattern;
    // length how many chars current input matches the pattern.
    // (ex: matchedLen is 2 if pattern === 'hoge' && input === 'ho')
    this.matchedLen = 0;
  }

  InputDetector.prototype.run = function () {
    const INPUT_ELEMENT_NAME = '.dz-clickable';
    return new Promise((resolve, reject) => {
      $(INPUT_ELEMENT_NAME).keydown(e => {
        this.read(e.key);
        if (this.detectsPattern()) {
          resolve(this.pattern);
          this.reset();
          // undind this function from DOM element
          $(INPUT_ELEMENT_NAME).off('keydown');
        }
      });
    })
  };

  InputDetector.prototype.read = function (char) {
    if (char === this.pattern[this.matchedLen]) {
      this.matchedLen++;
      return;
    }
    if (char === this.matchedLen[0]) {
      this.matchedLen = 1;
      return;
    }
    this.matchedLen = 0;
  };

  InputDetector.prototype.detectsPattern = function () {
    return this.matchedLen === this.pattern.length;
  };

  InputDetector.prototype.reset = function () {
    this.matchedLen = 0;
  }

  // alert when you mention to someone (ordinally for review) in WIP Merge Request!
  async function main() {
    const MENTION_PATTERN = '<@';
    const inputDetector = new InputDetector(MENTION_PATTERN);
    if (getTitle().isWip()) {
      while (1) {
        await inputDetector.run();
        showAlert();
      }
    }
  }

  main();
})();
