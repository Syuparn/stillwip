# stillwip

alert when you ask for review in WIP merge request

## About

You are writing a merge request.
You checked CI passed and mentioned your colleague to ask for review.
.. without removing `WIP`!

This script alerts when you write mention (like `<@syuparn>`) in WIP merge request.

*An apology: to be honest, this was the story of mine.*

## Requirements

- Tampermonkey

## usage

Copy `stillwip.js` to tampermonkey new script (don't forget to save ;).
This automatically works whele browsing GitLab MR pages.
